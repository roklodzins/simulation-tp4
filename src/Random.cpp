/**
 * @file Random.cpp
 * @brief implements the Random class
 * */


////////////////////////  Includes  ///////////////////////////

#include <iostream>
#include "Random.h"


/////////////////////////   Class   ///////////////////////////

/**
 * @brief instantiate the generator
 * @param[in] seed used for MT
 * */
Random::Random(s32 seed) : random(seed),
                           litter_size(3, 6),
                           nineth(0, 8),
                           sexe(0, 1),
                           maturity(5, 8),
                           month(0, 11),
                           dispatchs(0, 362244960),  // 495 * 792 * 924 -> uniform over modulo (for these values)
						   ones(0., 1.)
						   {}

						   
/**
 * @brief get the number of litter on a year
 * @return the number of litter in [4, 8]
 * @note probability: p(4)=p(8)=1/9, p(5)=p(7)=2/9, p(6)=3/9
 * */
s32 Random::get_litter_number(){
	const static s32 number[9] = {4, 5, 5, 6, 6, 6, 7, 7, 8};
    return number[nineth(random)];
}


/**
 * @brief get the litter size = the number of babies
 * @return the number of babies in [3, 6] (uniform distribution)
 * */
s32 Random::get_litter_size(){
    return litter_size(random);
}


/**
 * @brief get a litter dispatching layout over a year
 * @param[out] dispatch an out ref to an s32 array of size = the returned value,
 * an array containing the sorted list of months numbers where a litter exists (e.g.: {0, 1, 4, 5, 11})
 * @return the size of the out array = the number of litter
 * @note complexity ~O(1) by using cached pregenerated arrays
 * */
s32 Random::get_litter_dispatch(const s32 * &dispatch){
    const s32 num = get_litter_number();
	
	const s32 * disp = litter_dispatchs[num - 4];
	
	dispatch = disp + (num * (dispatchs(random) % litter_dispatch_size[num - 4]));
	
	return num;
}


/**
 * @brief get a litter dispatching layout over a year in binary format
 * @return the layout as a binary sequence, 1 = litter, 0 = no litter (bit 0 is current month)
 * @note complexity ~O(1) by using cached pregenerated arrays
 * */
u32 Random::get_litter_dispatch(){
    const s32 num = get_litter_number() - 4;
	
	return litter_dispatchs_binary[num][dispatchs(random) % litter_dispatch_size[num]];
}


/**
 * @brief get a month
 * @return a month in [0, 11]
 * */
s32 Random::get_birth_month() {
    return month(random);
}


/**
 * @brief get a sexe, probalility 50/50%
 * @return true if female, male otherwise
 * */
bool Random::is_female(){
    return sexe(random);
}


/**
 * @brief get the number of females in a children population, throw a binomial law p=50%
 * @param[in] children the total number of children
 * @return the number of females
 * @note alias of get_population_among
 * */
u64 Random::get_females(u64 children){
	return binomial_distribution<u64> (children)(random);
}


/**
 * @brief get the number of individuals in a population, throw a binomial law p=50%
 * @param[in] pop the population size
 * @return the number of individuals
 * */
u64 Random::get_population_among(u64 pop){
    return binomial_distribution<u64> (pop, 0.5)(random);
}


/**
 * @brief get the number of individuals in a population, throw a binomial law
 * @param[in] pop the population size
 * @param[in] proba the binomial parameter p in [0, 1]
 * @return the number of individuals
 * */
u64 Random::get_population_among(u64 pop, double proba){
    return binomial_distribution<u64> (pop, proba)(random);
}


/**
 * @brief get if a rabbit will die this month
 * @param[in] age the rabbit age
 * @return true if die, false otherwise
 * */
bool Random::will_die(s32 age){
    return ones(random) >= survive_rate[age];
}


/**
 * @brief get if a rabbit will die this year
 * @param[in] age the rabbit age
 * @return the remaining number of month to live or 12 if not die this year
 * */
s32 Random::will_die_year(s32 age){
	s32 i = 0;
	for(; i < 12; i++) {
		if (ones(random) >= survive_rate[age + i]) break; // survive_rate overflow avoided by last cell = 0
	}
	return i; // 12 -> not this year
}


/**
 * @brief get a random number in [0, 1], uniform distribution
 * @return a number in [0, 1]
 * */
double Random::get_ones(){
    return ones(random);
}


/**
 * @brief get the maturity age of a rabbit, uniform in [5, 8]
 * @return the maturity age
 * */
s32 Random::get_maturity(){
    return maturity(random);
}


const s32 * Random::litter_dispatchs[5] = {(const s32 *)litter_dispatch4,
										   (const s32 *)litter_dispatch5,
										   (const s32 *)litter_dispatch6,
										   (const s32 *)litter_dispatch7,
										   (const s32 *)litter_dispatch8};
