/**
 * @file utils.cpp
 * @brief implementation of utilities functions
 * @copyright Romain KLODZINSKI - ISIMA F2 ZZ2 - (c) 2023
 * */


////////////////////////  Includes  ///////////////////////////

#include "utils.h"
#include <ctime>


///////////////////////   Functions   /////////////////////////

/**
 * @brief compute the elapsed time, with nanosecond resolution, between 2 calls
 * @return the elapsed time
 * @note the first call may no be trusted
 * */
double time_it() {
	static struct timespec last = {};
	struct timespec now = {};
	
	clock_gettime(CLOCK_REALTIME, &now);
	
	double elapsed = (double)(now.tv_sec - last.tv_sec) + (double)(now.tv_nsec - last.tv_nsec) / 1000000000.;
	last = now;
	return elapsed;
}
