/**
 * @file SimpleSimulation.cpp
 * @brief implement the SimpleSimulation class
 * */


////////////////////////  Includes  ///////////////////////////

#include <iostream>
#include "SimpleSimulation.h"

using namespace std;


/////////////////////////   Class   ///////////////////////////

/**
 * @brief instanciate a simulation
 * @param[in] seed the seed used by the RNG
 * */
SimpleSimulation::SimpleSimulation(s32 seed) : random(seed) {}


/**
 * @brief run the simulation
 * @param[in] years the number of years to simulate
 * @param[out] out the output file
 * */
void SimpleSimulation::run(s32 years, ostream& out){
	
    for(s32 year = 0; year < years; year++){
        out << "Year number : " <<  year + 1 << endl;

        for(s32 month = 0; month < 12; month++){
            run_month();
            out << "\tMonth number " <<  month + 1 << " : ";
            out << "\tAssociated Fibonacci = " <<  fibo1;
            out << "\t\tTotal Rabbits = " << number_growing_females + number_adults_females + number_males << endl;
            fibo2 += fibo1;
            fibo1 = fibo2 - fibo1;
        }
    }
}


/**
 * @brief run a month of simulation
 * */
void SimpleSimulation::run_month(){

	u64 number_children_females = random.get_females(number_adults_females << 1);
	u64 number_children_males = (number_adults_females << 1) - number_children_females;
		
    // Young rabbits become adults
    number_adults_females += number_growing_females;
	
    // All new birth rabbits start their growing
    number_growing_females = number_children_females;
	
    // Males become adults directly for simplification
    number_males += number_children_males;
	
	/// previous impl. using random functions limited on s32 -> required some magic to deal with u64 precision
	/*
	for (u64 i = number_adults_females >> 31; i; i--) {
		number_children_females += (u64)generatorRandom.get_females(0x7fffffff) // upper u33 part of u64 -> 0x1_0000_0000 * 2
				                 + (u64)generatorRandom.get_females(0x7fffffff)
				                 + (u64)generatorRandom.get_females(2); // complete to 0x80000000 (2 times)
	}

	number_children_females += (u64)generatorRandom.get_females((s32)(number_adults_females & 0x7fffffff))  // s32 part of u64
			                 + (u64)generatorRandom.get_females((s32)(number_adults_females & 0x7fffffff)); // x2 because 2 children by female
	
	*/
}
