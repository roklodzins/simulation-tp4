/**
 * @file ObjectSimulation.cpp
 * @brief implement the ObjectSimulation class
 * */


////////////////////////  Includes  ///////////////////////////

#include "ObjectSimulation.h"


/////////////////////////   Class   ///////////////////////////

/**
 * @brief generate the initial population, randomly spread on all Rings
 * @param[in] pop the population size
 * */
void ObjectSimulation::initiate_population(u64 pop) {
	total_births = rabbits = pop;
	
	for (s32 i = 0; pop; i++) {
		u64 n = random.get_population_among(pop, Random::survive_rate_year[i % 192] / 2.);
		pop -= n;
		
		for (; n; n--) {
			auto r = new Rabbit(random, i);
			RingsRaw(rings, 4 - r->female, 0)++; // count the rabbit sexe
			listRabbit.push_back(r);
		}
	}
}


/**
 * @brief run 1 month of simulation
 * */
void ObjectSimulation::run_month() {
	
	list<Rabbit *> to_remove;
	list<Rabbit *> to_add;
	
	births = 0;
	
	for (auto rabbit: listRabbit) {
		//aging + preparation kittens
		births += rabbit->run_month(random);
		
		//death
		if (rabbit->is_dying(random)) to_remove.push_back(rabbit);
		
	}
	
	u64 effective_births = random.get_population_among(births, Random::survive_rate[0]); // month-0 death
	total_births += births;
	total_deaths += births - effective_births;
	rabbits += effective_births;
	
	for (; effective_births; effective_births--) {
			auto r = new Rabbit(random);
			RingsRaw(rings, 4 - r->female, 0)++; // count the rabbit sexe
			listRabbit.push_back(r);
	}
	
	for (auto suppr_rabbit: to_remove) { // slow
		total_deaths++;
		rabbits--;
		RingsRaw(rings, 4 - suppr_rabbit->female, 0)--; // count the rabbit sexe
		listRabbit.remove(suppr_rabbit);
		delete suppr_rabbit;
	}
}


/**
 * @brief print all rabbits properties
 * @note debug
 * */
void ObjectSimulation::write() {
	for (auto rabbit: listRabbit) rabbit->write();
}


/**
 * @brief clear the rabbits list
 * */
ObjectSimulation::~ObjectSimulation() {
	for (auto suppr: listRabbit) delete suppr;
	listRabbit.clear();
}
