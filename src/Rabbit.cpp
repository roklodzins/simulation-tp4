/**
 * @file Rabbit.cpp
 * @brief implement the Rabbit class
 * */


////////////////////////  Includes  ///////////////////////////

#include <iostream>

#include "Rabbit.h"
#include "Random.h"


/////////////////////////   Class   ///////////////////////////

/**
 * @brief instantiate a Rabbit
 * @param[in, out] rng the RNG used to generate the rabbit
 * @param[in] age the age of the rabbit (0 by default)
 * */
Rabbit::Rabbit(Random& rng, u8 age) : age(age) {
	s32 maturity = rng.get_maturity();
	if (age >= maturity) {
		predicted_birth = 2;
	} else {
		predicted_birth = 1 << (maturity - 1);
	}
	
	female = rng.is_female();
}


/**
 * @brief simulate 1 month
 * @param[in, out] rng the RNG
 * @return the number of children to give birth to
 * */
s32 Rabbit::run_month(Random &rng) {
	
	age++;
	
	if (female) {
		
		predicted_birth >>= 1;
		
		if (predicted_birth == 1) { // bit limit reached -> regenerate
			predicted_birth = rng.get_litter_dispatch() | 0x1000; // 0x1000 = 1 << 12 = year limit
		}
		
		if (predicted_birth & 1) return rng.get_litter_size();
	}
	
	return 0;
}


/**
 * @brief test if the rabbit will die this month
 * @param[in, out] rng the RNG
 * @return true if dying, false otherwise
 * */
bool Rabbit::is_dying(Random &rng) const {
	return rng.will_die(age);
}


/**
 * @brief print the Rabbit properties
 * @note debug
 * */
void Rabbit::write() const {
	std::cout << "Age : " << (int) age << " \t| isFemale : " << ((female) ? "yes" : "no ") << " | birth : " << predicted_birth << endl;
}
