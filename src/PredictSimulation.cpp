/**
 * @file PredictSimulation.cpp
 * @brief implement the PredictSimulation class
 * */
 

////////////////////////  Includes  ///////////////////////////

#include "PredictSimulation.h"
#include <iostream>

using namespace std;


////////////////////////   Methods   //////////////////////////

/**
 * @brief print the births prediction
 * @param[in] offseted align the printing on the sliding window offset if true
 * @note debug
 * */
void PredictSimulation::print_prebirth(bool offseted) {
	for (int x = 0; x < 12; x++) {
		u64 val = offseted ? SlidingWindowAccess(prebirth, x): prebirth.slide[x];
		cout << (val ? "\x1b[38;2;200;135;0m": "\x1b[38;2;0;172;128m") << val << " ";
	}
	cout << "\x1b[0m" << endl;
}


/**
 * @brief print the deaths prediction
 * @note debug
 * */
void PredictSimulation::print_predeath() {
	for (int x = 0; x < 5; x++) {
		for (int v = 0; v < 192; v++) {
			u64 val = DeathRingsAccess(predeath, 0, x, v);
			cout << (val ? "\x1b[38;2;200;135;0m": "\x1b[38;2;80;80;80m") << val << " ";
		}
		cout << "\x1b[0m" << endl;
	}
}


/**
 * @brief run 1 month of simulation
 * */
void PredictSimulation::run_month() {
	births = 0, deaths = 0;

    /// Time Forward

    date++;
    RingsAging(rings);
	DeathRingsSlide(predeath);
	
	
	/// Pre-Generate Birth & Adult Female Death
	
    for (s32 r = 0; r < 4; r++) { /// iter rings by maturity
		
        s32 maturity_month = r + 5;
        
        forRabbitByMatureMonth(maturity_month, i) { /// iter over females which maturity month is the current month
			
			u64 females = RingsFemale(rings, r, i);
			
			for (u64 f = females; f; f--) { /// for each female
			
				const s32 * litter_dispatch;
				s32 size = random.get_litter_dispatch(litter_dispatch); /// get the litter 'layout' of the year
				s32 death = random.will_die_year(i);
				
				if (death < 12) DeathRingsRegister(predeath, death, r, i); /// register the death date (this year)
				
                for (s32 litter = 0; litter < size; litter++) { /// for each female's litter

					if (litter_dispatch[litter] > death) break; /// cancel births after mother death, obviously
					
                    SlidingWindowAccess(prebirth, litter_dispatch[litter]) += random.get_litter_size(); /// reg. births
                }
            }
		}
    }
	
	
	/// Manage Birth of this month
	
	births = SlidingWindowAccess(prebirth, 0);
	SlidingWindowAccess(prebirth, 0) = 0; // reset
    SlidingWindowSlide(prebirth);

	u64 females = random.get_females(births);
	u64 processed;
	
	RingsMale(rings, 0) = births - females;
	
	processed = RingsFemale(rings, 0, 0) = random.get_population_among(females, 0.25);
	females -= processed;
	
	processed = RingsFemale(rings, 1, 0) = random.get_population_among(females, 0.33);
	females -= processed;
	
	processed = RingsFemale(rings, 2, 0) = random.get_population_among(females, 0.5);
	females -= processed;
	
	RingsFemale(rings, 3, 0) = females;
	
	
	/// Manage Male Death (Adult + Children)
	
	forRabbitByBornMonth(i) {
		for (u64 males = RingsMale(rings, i); males; males--) {
			s32 month = random.will_die_year(i);
			if (month < 12) DeathRingsRegister(predeath, month, 4, i);
		}
	}
	
	
	/// Manage Female children Death
	
	for (s32 r = 0; r < 4; r++) {
        s32 maturity_month = r + 5;
		forRabbitChildren(maturity_month, i) {
			for (females = RingsFemale(rings, r, i); females; females--) {
				if (random.will_die(i)) DeathRingsRegister(predeath, 0, r, i);
			}
		}
	}
	
	
	/// Process deaths
	for (s32 r = 0; r < 5; r++){
		forRabbits(i) {
			u64& d = DeathRingsAccess(predeath, 0, r, i);
			RingsRaw(rings, r, i) -= d;
			deaths += d;
			d = 0;
		}
	}
	
	total_births += births;
	total_deaths += deaths;
	rabbits += births - deaths;
}
