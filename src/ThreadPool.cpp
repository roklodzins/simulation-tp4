/**
 * @file ThreadPool.cpp
 * @brief implement the ThreadPool class
 * */


////////////////////////  Includes  ///////////////////////////

#include "ThreadPool.h"
#include <iostream>
#include <fstream>


/////////////////////////   Class   ///////////////////////////

/**
 * @brief instantiate a ThreadPool
 * */
ThreadPool::ThreadPool() : tasks_mutex() {
	cpu = thread::hardware_concurrency();
	if (cpu < 2) cpu = 2;
	cpu--; // remove the main thread
	
	cout << "Using " << cpu << " CPU cores" << endl;
	pool = new thread[cpu];
}


/**
 * @brief push a task to the ThreadPool
 * */
void ThreadPool::push(Simulation * simu, s32 years, s32 id) {
	tasks.push((Task){simu, years, id});
}


/**
 * @brief run/enable all tasks in the ThreadPool
 * @param[in] blocking if true, wait for all tasks to finish (see wait)
 * */
void ThreadPool::run(bool blocking) {
	task_num = tasks.size();
	task_done = 0;
	first = false;
	
	for (u32 i = 0; i < cpu; i++) {
		pool[i] = thread(&ThreadPool::thread_run, this);
	}
	
	if (blocking) wait(); // wait for all threads to finish
}


/**
 * @brief wait for all tasks to finish
 * */
void ThreadPool::wait() {
	for (u32 i = 0; i < cpu; i++) {
		if (pool[i].joinable()) pool[i].join();
	}
}


/**
 * @brief task runner (threads use)
 * */
void ThreadPool::thread_run() {
	while (true) {
		Task task;
		
		{ // mutex scope block
			lock_guard<mutex> lock(tasks_mutex);
			
			if (tasks.empty()) break;
			
			task = tasks.front();
			tasks.pop();
		}
		
		task.simu->run(task.years, 10, (bool)task.id);
		task_done++;
		
		if (!task.id) {
			first = true;
			cout << "Tasks " << task_done << " / " << task_num << " done" << flush;
		}
		else if (first) {
			cout << "\r\033[KTasks " << task_done << " / " << task_num << " done" << flush;
		}
		
		delete task.simu;
	}
}


/**
 * @brief destroy a ThreadPool
 * */
ThreadPool::~ThreadPool() {
	delete[] pool;
}
