/**
 * @file Simulation.cpp
 * @brief implement the Simulation class
 * */


////////////////////////  Includes  ///////////////////////////

#include <iostream>
#include <fstream>
#include "Simulation.h"


/////////////////////////   Class   ///////////////////////////

/**
 * @brief instanciate a simulation
 * @param[in] seed the seed used for the RNG
 * @param[out] out the output file to store each steps states (months)
 * */
Simulation::Simulation(s32 seed, ofstream& out) : random(seed), out(out) {}


/**
 * @brief instanciate a simulation
 * @param[in] seed the seed used for the RNG
 * @param[out] filename the output filename to store each steps states (months)
 * */
Simulation::Simulation(s32 seed, const string& filename) : random(seed), out(*new ofstream(filename, ios::binary)), internal_file(true) {}


/**
 * @brief generate the initial population, randomly spread on all Rings
 * @param[in] pop the population size
 * */
void Simulation::initiate_population(u64 pop){
	
	rabbits = births = total_births = pop;
	
	for (s32 i = 0; pop; i++) {
		u64 n = random.get_population_among(pop, Random::survive_rate_year[i % 192] / 2.);
		RingsMale(rings, i) += n;
		pop -= n;
	}
	
	forRabbits(i) {
		RingsFemale(rings, 0, i) = random.get_population_among(RingsMale(rings, i), 0.5);
		RingsMale(rings, i) -= RingsFemale(rings, 0, i);
		
		for (s32 r = 3; r; r--) {
			RingsFemale(rings, r, i) = random.get_population_among(RingsFemale(rings, 0, i), 1. / (r + 1));
			RingsFemale(rings, 0, i) -= RingsFemale(rings, r, i);
		}
	}
}


/**
 * @brief print the rings, aka the number of rabbits per age & sexe (& maturity for females)
 * @param[in] offseted align the printing on the ring rotation offset if true
 * @note debug
 * */
void Simulation::print_rings(bool offseted) {
	for (int x = 0; x < 5; x++) {
		for (int v = 0; v < 192; v++) {
			u64 val = offseted ? RingsRaw(rings, x, v): rings.rings[x][v];
			cout << (val ? "\x1b[38;2;200;135;0m": "\x1b[38;2;0;172;128m") << val << " ";
		}
		cout << "\x1b[0m" << endl;
	}
}


/**
 * @brief save the simulation state as raw binary format in the output file
 * @note raw format: Rings rings + u64 total_births + u64 total_deaths + u64 rabbits
 * */
void Simulation::save_state() {
	out.write((const char *)&rings, sizeof rings);
	out.write((const char *)&total_births, sizeof total_births);
	out.write((const char *)&total_deaths, sizeof total_deaths);
	out.write((const char *)&rabbits, sizeof rabbits);
	out.flush();
}


/**
 * run the simulation
 * @param[in] years the number of simulated years to run (simulation resolution is month)
 * @param[in] pop the initial population size (10 by default)
 * @param[in] silent no log to stdout if true (false by default)
 * */
void Simulation::run(s32 years, u64 pop, bool silent) {
	if(!silent) time_it();
	
	initiate_population(pop);
	save_state();
	
	if(!silent) {
		cout << "Init " << " > Rabbits: " << rabbits << "\tBirths: " << total_births << "\tDeaths: " << total_deaths << "\tIn: " << time_it() << " seconds" << endl << endl;
		print_rings(true);
	}
	
	for (s32 y = 1; y <= years; y++) {
		if(!silent) cout << "Year " << y << endl;
		
		for (s32 m = 1; m <= 12; m++) {
			
			run_month();
			save_state();
			
			if(!silent) {
				cout << "Month " << m << " > Rabbits: " << rabbits << "\tBirths: " << total_births << "\tDeaths: " << total_deaths << "\tIn: " << time_it() << " seconds" << endl;
				print_rings(true);
				cout << endl;
			}
		}
	}
}


/**
 * @brief run 1 month of simulation
 * */
void Simulation::run_month() {
	births = 0, deaths = 0;
	
    /// Time Forward

    date++;
    RingsAging(rings);


	/// Handle Births
	
    for (s32 r = 0; r < 4; r++) { /// Handle Births: iter rings by maturity

        s32 maturity_month = r + 5;
	    
	    forRabbitFertileAdult(maturity_month, random.last_fertility_month, i) { /// iter over females
	    
			u64& females = RingsFemale(rings, r, i);
			
			u64 litters = random.get_population_among(females, Random::fertility_rate[i]); /// get num of litters
			
			u64 babies = random.get_population_among(litters, 0.25); /// dispatch in number of babies per litter size
			litters -= babies;
		    births += babies * 3;
			
			babies = random.get_population_among(litters, 0.33);
			litters -= babies;
		    births += babies * 4;
			
			babies = random.get_population_among(litters, 0.5);
			litters -= babies;
		    births += babies * 5;
		    
		    births += litters * 6;
		}
    }
	
	
	/// Handle Births Generation

	u64 females = random.get_females(births); // take a binomial half
	u64 processed;
	
	RingsMale(rings, 0) = births - females;
	
	RingsFemale(rings, 0, 0) = processed = random.get_population_among(females, 0.25); // take a binomial quarter
	females -= processed;
	
	RingsFemale(rings, 1, 0) = processed = random.get_population_among(females, 0.33); // take the second binomial quarter
	females -= processed;
	
	RingsFemale(rings, 2, 0) = processed = random.get_population_among(females, 0.5);  // take the third binomial quarter
	females -= processed;
	
	RingsFemale(rings, 3, 0) = females;  // take what's remaining
	
	
	/// Handle Deaths
	
	forRabbits(i) {
		
		for (s32 r = 0; r < 5; r++) {
			
			u64& lives = RingsRaw(rings, r, i);
			double mortality = Random::survive_rate[i] * Random::season_survive_rate[date % 12];
			
			u64 survives = random.get_population_among(lives, mortality);
			
			deaths += lives - survives;
			
			lives = survives;
		}
	}
	
	
	/// Update State
	
	total_births += births;
	total_deaths += deaths;
	rabbits += births - deaths;
}


/**
 * @brief destroy the simulation and close the output file
 * */
Simulation::~Simulation() {
	if (out.is_open()) out.close();
	if (internal_file) delete &out;
}
