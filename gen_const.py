import matplotlib.pyplot as plt
import numpy as np
import math, os


def clist(py: list) -> str:
    """Convert a Python list to a string representing a C array"""
    return str(py).replace("[", "{").replace("]", "}")


if __name__ == "__main__":

    # Generate all litters layouts in a year
    c = []
    cb = []

    for L in range(4, 9):
        cb.append([])
        c.append([[u for u in range(12) if i & (1 << u)] for i in range(1 << 12) if i.bit_count() == L and (cb[-1].append(i) or True)])

    print(f"constexpr static const s32 litter_dispatch_size[5] = {clist(list(map(len, c)))};\n")

    for n, i in enumerate(c):
        print(f"constexpr static const s32 litter_dispatch{n + 4}[{len(i)}][{n + 4}] = {clist(i)};")

    for n, i in enumerate(cb):
        print(f"constexpr static const u32 litter_dispatch_binary{n + 4}[{len(i)}] = {clist(i)};")


    # Generate survive rates (in 1 month) per age in month

    pbc = 0.819 # 0.35 ** (1 / 12)
    pba = 0.6 ** (1 / 12)

    pb = []

    p = pbc
    inc = (pba - pbc) / 6
    
    for i in range(192):
        pb.append(p)
        p += inc
        if i == 5: inc = 0
        if i == 12 * 10 - 1:
            inc = -pba / (6 * 12 - 1)

    pb[-1] = 0

    cumul = [1]
    for i in pb:
        cumul.append(cumul[-1] * i)
    cumul.pop(0)
    print(f"constexpr static const double survive_rate[{len(pb)}] = {clist(pb)};")


    # Generate survive rates (in a whole year) per age in month
    
    pby = []
    for i in range(192):
        r = 1
        for u in range(12):
            r *= pb[min(i+u,191)]
        pby.append(r)

    print(f"constexpr static const double survive_rate_year[{len(pby)}] = {clist(pby)};")


    # Generate fertility rates per age in month

    lp = [0] * 5 + [0.5] * (12 * 5) + list(np.linspace(0.5, 0, 12*2))
    last = len(lp) - 2
    lp.extend([0] * (192 - len(lp)))


    print(f"constexpr static const double fertility_rate[{len(lp)}] = {clist(lp)};")
    print(f"constexpr static const s32 last_fertility_month = {last};")


    # Generate season survive rates
    N = lambda s, x: (math.exp(-(x/s) ** 2 / 2)) / (s * (2 * math.pi) ** 0.5)
    S = 1.1
    pbw = [1] * 12
    for i in range(4):
        pbw[i] -= S * N(S, i)
        if i: pbw[-i] = pbw[i]

    print(f"constexpr static const double season_survive_rate[{len(pbw)}] = {clist(pbw)};")


    # Plot data

    try: os.mkdir("img")
    except: pass
    
    plt.figure(figsize=(32, 10))

    ROWS = 1
    COLS = 3
    IDX = 1
    
    
    plt.subplot(ROWS, COLS, IDX)
    plt.tight_layout(pad=3)
    plt.margins(0.015, tight=True)
    plt.plot(range(len(pbw)), pbw)
    plt.legend(['Multiplicator'], prop={'size': 14})
    plt.xlabel("Months")
    plt.ylabel("Probabilities")
    plt.title(f"Season mortality multiplicator by month (1 - S * N(January, {S}))")
    IDX += 1
    
    plt.subplot(ROWS, COLS, IDX)
    plt.tight_layout(pad=3)
    plt.margins(0.015, tight=True)
    plt.plot(range(len(pb)), pb)
    plt.plot(range(len(cumul)), cumul)
    plt.plot(range(len(pby)), pby)
    plt.legend(['Probability', 'Cumulated probability', 'Probability over 1 Year'], prop={'size': 14})
    plt.xlabel("Ages")
    plt.ylabel("Probabilities")
    plt.title(f"Survive probability by age in month")
    IDX += 1
    
    plt.subplot(ROWS, COLS, IDX)
    plt.tight_layout(pad=3)
    plt.margins(0.015, tight=True)
    plt.plot(range(len(lp)), lp)
    plt.legend(['Fertility'], prop={'size': 14})
    plt.xlabel("Ages")
    plt.ylabel("Probabilities")
    plt.title(f"Fertility probability by age in month")
    IDX += 1

    
    plt.savefig("img/proba.png")
    plt.show()
