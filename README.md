# Simulation TP4

This is the repository of the TP4 of the Simulation course at ISIMA (Second year, F2 ZZ2).

The subject can be found [here](https://perso.isima.fr/~dahill/Simu-ZZ2/Lab%20%23%204%20-%20Rabbit%20Population%20growth.pdf) [11/2023]

The report can be found in this repository as "Rapport TP4.pdf" (French).


## Description

The purpose of this TP is to simulation rabbits populations:
- The basic implementation is the subproject TP4Fibo
- Advanced implementations are the subproject TP4


## Build

To build executables named TP4 (or TP4.exe) and TP4Fibo, just do:
```bash
cmake . && cmake --build .
```

## License MIT
Copyright © 2023 Romain KLODZINSKI & Evahn LE GAL

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the “Software”), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
