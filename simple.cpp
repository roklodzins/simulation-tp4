/**
 * @file simple.cpp
 * @brief main program for the fibo-like simulation
 * */
 

////////////////////////  Includes  ///////////////////////////

#include <iostream>
#include <fstream>
#include <filesystem>

#include "SimpleSimulation.h"

using namespace std;
namespace fs = filesystem;


////////////////////////   Program   //////////////////////////

/**
 * @brief execute 100 fibo-like simulations writing to out/i.txt (+1 to terminal)
 * @return 0
 * */
int main(){

    mt19937_64 random(0); // used to generate determ. seeds
	
	
	/// run 1 simu, logging to the terminal
	
	SimpleSimulation simu((s32)random());
    simu.run(8, cout);
	
	
	/// run 100 simu, logging to file

	cout << endl <<endl;
	if (!filesystem::is_directory("out")) filesystem::create_directory("out"); // try to create the out/ dirrectory
	
	for (s32 i = 0; i < 100; i++) {
		ofstream file("out/" + to_string(i) + ".txt");
		
		SimpleSimulation simus((s32) random());
		simus.run(8, file);
		file.close();
		
		cout << "|" << flush;
	}
	
	cout << endl;

    return 0;
}
