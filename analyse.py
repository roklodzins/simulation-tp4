import matplotlib.pyplot as plt
from os.path import *
import os, numpy as np
import ctypes

u64 = ctypes.c_uint64
s32 = ctypes.c_int32

DEBUG = 0


class CData(ctypes.Structure):
    _fields_ = [("rings", u64 * 192 * 5),
                ("offset", s32),
                ("births", u64),
                ("deaths", u64),
                ("rabbits", u64)]


class SimuType:
    """Represent 1 simulation type"""

    def __init__(self, name, prefix):

        self.name = name
        self.prefix = prefix
        self.data = []
        i = 0

        print("Load", name, "modele's data")
        
        while isfile(fn := f"{i}{prefix}.bin"):
            
            with open(fn, "rb") as f:
                
                print("Load", fn, '...')
                self.data.append(Data(f.read()))

            if DEBUG and i > 5: break  # fast start for debug test 
            i += 1
            
        self.num = i

    def __getitem__(self, idx): return self.data[idx]
    def __len__(self): return len(self.data)
    def __iter__(self): return iter(self.data)

    def __add__(self, o):
        return self.data + o.data

        

class Data:
    """Represent 1 whole simulation"""

    def __init__(self, data):
        self.rings        = []  # stores all rings groups, indiced by month

        self.raw_mfemales = []  # stores raw info indiced by month
        self.raw_females  = []  # stores raw info indiced by month
        self.raw_males    = []  # stores raw info indiced by month
        self.raw_rabbits  = []  # stores raw info indiced by month

        self.all_births   = []  # stores count info indiced by month
        self.all_deaths   = []  # stores count info indiced by month
        self.births       = []  # stores count info indiced by month
        self.deaths       = []  # stores count info indiced by month
        self.all_rabbits  = []  # stores count info indiced by month
        self.all_females  = []  # stores count info indiced by month
        self.all_males    = []  # stores count info indiced by month
        self.delta        = []  # stores count info indiced by month
        self.all_delta    = []  # stores count info indiced by month

        sz = ctypes.sizeof(c := CData())
        self.month = len(data) // sz
        

        assert self.month == len(data) / sz

        for i in range(self.month):

            ctypes.memmove(ctypes.byref(c), dt := data[sz * i: sz * (i + 1)], sz)


            self.rings.append(np.array(rings := [[i[(u + c.offset) % 192] for u in range(192)] for i in c.rings],
                                       dtype="uint64"))

            self.raw_mfemales.append(np.array(rings[:-1], dtype="uint64"))
            self.raw_females.append(f := self.raw_mfemales[-1].sum(axis=0))
            self.raw_males.append(m := np.array(rings[-1], dtype="uint64"))
            self.raw_rabbits.append(f+m)

            self.all_females.append(f.sum())
            self.all_males.append(m.sum())

            self.all_births.append(c.births)
            self.all_deaths.append(c.deaths)
            self.all_rabbits.append(c.rabbits)

            if i:
                self.births.append(c.births - self.all_births[-2])
                self.deaths.append(c.deaths - self.all_deaths[-2])
            else:
                self.births.append(c.births)
                self.deaths.append(c.deaths)

        self.rings        = np.array(self.rings       , dtype="uint64")
        self.raw_mfemales = np.array(self.raw_mfemales, dtype="uint64")
        self.raw_females  = np.array(self.raw_females , dtype="uint64")
        self.raw_rabbits  = np.array(self.raw_rabbits , dtype="uint64")
        self.raw_males    = np.array(self.raw_males   , dtype="uint64")
        self.all_births   = np.array(self.all_births  , dtype="uint64")
        self.all_deaths   = np.array(self.all_deaths  , dtype="uint64")
        self.births       = np.array(self.births      , dtype="uint64")
        self.deaths       = np.array(self.deaths      , dtype="uint64")
        self.rabbits      = np.array([10] + self.all_rabbits[:-1], dtype="uint64") # pop at month' begin
        self.all_rabbits  = np.array(self.all_rabbits , dtype="uint64") # pop at month end
        self.all_females  = np.array(self.all_females , dtype="uint64")
        self.all_males    = np.array(self.all_males   , dtype="uint64")

        self.delta        = np.array(self.births, dtype="int64") - np.array(self.deaths, dtype="int64")



ROWS = 2
COLS = 4
IDX = 1


def plot(title: str, legend: list, color: list, plot: list, axis: list, curve: int):
    """Plot"""
    
    global ROWS, COLS, IDX
    
    def _subplot(sp, log):
        plt.subplot(*sp)
        plt.tight_layout(pad=5)
        plt.margins(0.015, tight=True)
        
        # plot all curves
        for i in range(curve):
            a = 1 - i / (2 * curve)  # define sliding color
            alpha = 0.2 if i else 1  # alpha = 1 when i = 0 -> color used by the legend

            for c, v in zip(color, plot):
                plt.plot(*v(i), color=c(a, alpha))

        plt.legend(legend, prop={'size': 14})

        plt.title(title + log * " (Log10 scale)")
        
        if log:
            plt.yscale("log", base=10)
        else:
            plt.yscale("linear")

        plt.xlabel(axis[0])
        plt.ylabel(axis[1])

    _subplot((ROWS, COLS, IDX), False)
    _subplot((ROWS, COLS, IDX + COLS), True)
    IDX += 1



if __name__ == "__main__":
    os.chdir(dirname(__file__))

    # Find the out/ dir

    dirs = []

    for i in filter(isdir, os.listdir()):
        if i == "out":
            os.chdir("out")
            break
        else:
            dirs.append(i)
    else:
        for i in dirs:
            if isdir(d := f"{i}/out"):
                os.chdir(d)
                break
        else:
            print("out/ dir not found! (Did the simple simulation run?)")

    print("Current working directory:", os.getcwd())

    # Load data

    extended = SimuType("Extended", "")
    predict = SimuType("Predict", ".predict")
    objects = SimuType("Objects", ".object")

    with np.errstate(divide='ignore', invalid='ignore'): # remove div/0 warning
        
        for simu in (extended, predict, objects):

            IDX = 1

            fig = plt.figure(figsize=(32, 10))  # set plot size
            
            #(f'Data analysis for {simu.name} modele')
                
            #doc plot(title: str, legend: list, color: list, plot: list, axis: list, curve: int)
            
            plot(f"Evolution of {simu.num} rabbits populations over {simu[0].month // 12} years",
                 ["Rabbits"],
                 [lambda a, t: (0, a, a, t)],
                 [lambda i: (range(simu[i].month), simu[i].all_rabbits)],
                 ["Months", "Rabbits"],
                 simu.num)
            
            plot(f"Evolution of {simu.num} rabbits populations' sexe ratio over {simu[0].month // 12} years",
                 ["Ratios Females / Males"],
                 [lambda a, t: (a, a * 0.5859375, a * 0.859375, t)],
                 [lambda i: (range(simu[i].month), simu[i].all_females / simu[i].all_males)],
                 ["Months", "Ratio"],
                 simu.num)
            
            plot(f"Evolution of {simu.num} rabbits populations' monthly births & deaths over {simu[0].month // 12} years",
                 ["Deaths / Pop", "Births / Pop"],
                 [lambda a, t: (a, 0, 0, t),
                  lambda a, t: (0, a, 0, t)],
                 [lambda i: (range(simu[i].month), simu[i].deaths / simu[i].rabbits),
                  lambda i: (range(simu[i].month), simu[i].births / simu[i].rabbits)],
                 ["Months", "Rabbits"],
                 simu.num)
            
            plot(f"Evolution of {simu.num} rabbits populations' births/deaths ratio over {simu[0].month // 12} years",
                 ["Ratios Births / Deaths"],
                 [lambda a, t: (a, a * 0.5, a * 0.153, t)],
                 [lambda i: (range(simu[i].month), simu[i].births / simu[i].deaths)],
                 ["Months", "Ratio"],
                 simu.num)

    try: os.mkdir("img")
    except: pass
    
    plt.savefig("img/stats.png")
    plt.show()
