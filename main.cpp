/**
 * @file main.cpp
 * @brief main program for the main simulation
 * */
 

////////////////////////  Includes  ///////////////////////////

#include <iostream>
#include <fstream>
#include <filesystem>

#include "PredictSimulation.h"
#include "ObjectSimulation.h"
#include "ThreadPool.h"

using namespace std;
namespace fs = filesystem;


////////////////////////   Program   //////////////////////////

/**
 * @brief execute 300 simulations writing results to out/ (100 of each type)
 * @return Success (0)
 * */
int main(){
	
    mt19937_64 random(42); // used to generate determ. seeds
	
	/// run 100 simu, logging to file

	cout << endl <<endl;
	if (!filesystem::is_directory("out")) filesystem::create_directory("out"); // try to create the out/ dirrectory
	
	ThreadPool threadPool;
	
	
	for (s32 i = 0; i < 100; i++) {
		threadPool.push(new Simulation((s32) random(), "out/" + to_string(i) + ".bin"), 50, i);
	}
	
	threadPool.run();
	
	
	for (s32 i = 0; i < 100; i++) {
		threadPool.push(new PredictSimulation((s32) random(), "out/" + to_string(i) + ".predict.bin"), 7, i);
	}
	
	threadPool.run();
	
	
	for (s32 i = 0; i < 100; i++) {
		threadPool.push(new ObjectSimulation((s32) random(), "out/" + to_string(i) + ".object.bin"), 4, i);
	}
	
	threadPool.run();
	
	cout << endl;
	
    return 0;
}
