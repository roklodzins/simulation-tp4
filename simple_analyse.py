import matplotlib.pyplot as plt
from os.path import *
import os, numpy as np


if __name__ == "__main__":
    os.chdir(dirname(__file__))


    # Find the out/ dir

    dirs = []

    for i in filter(isdir, os.listdir()):
        if i == "out":
            os.chdir("out")
            break
        else: dirs.append(i)
    else:
        for i in dirs:
            if isdir(d := f"{i}/out"):
                os.chdir(d)
                break
        else:
            print("out/ dir not found! (Did the simple simulation run?)")

    print("Current working directory:", os.getcwd())


    # Load data

    data = []
    fibo = []
    overflow = []

    i = 0
    while isfile(fn := f"{i}.txt"):

        with open(fn, "r") as f:
            data.append([])

            for line in f.readlines():

                if "=" in line:
                    _, fb, val = line.split(" = ")
                    if not i: fibo.append(int(fb.split('\t')[0].strip()))
                    data[-1].append(int(val))
            c = -1

            for n, v in enumerate(data[-1]):

                if v < c:
                    overflow.append(n)
                    break
                c = v

        i += 1

    filenum = i

    data = np.array(data)
    fibo = np.array(fibo)
    overflow = np.array(overflow)

    plt.figure(figsize=(32, 10))  # set plot size

    def plot(log: bool):
        x = range(len(fibo))

        plt.subplot(121 if log else 122)
        plt.tight_layout(pad = 5)
        plt.margins(0.015, tight=True)

        # plot the month of the first u64 overflow
        plt.axvline(x = overflow.min(), color=(1, 0, 0, 1), ls=':')

        plt.plot([], [], color=(0, 0, 0, 1), linestyle='dashed')  # fictive plot for acurate legend color

        # plot all curves
        for i in range(filenum):
            a = 1 - i / 200  # define sliding color
            alpha = 0.2 if i else 1  # alpha = 1 when i = 0 -> color used by the legend

            plt.plot(x, data[i, :], color=(0, a, a, alpha))

        # plot real fibo: the theoretical "average"
        plt.plot(x, fibo, color=(0, 0, 0, 1), linestyle='dashed')

        # plot the mean (not very interesting)
        # plt.plot(x, data[:].mean(axis=0), color=(1, 0, 0, 1), linestyle='dashed')


        plt.legend(['U64 Overflow', 'Real Fibonacci', 'Rabbits Populations'], prop={'size': 20})

        plt.title(f"Evolution of {filenum} rabbits populations over 8 years" + log * " (Log2 scale)")
        if log: plt.yscale("log", base=2)
        else: plt.yscale("linear")

        plt.xlabel("Months")
        plt.ylabel("Rabbits")


    plot(True)
    plot(False)

    try: os.mkdir("img")
    except: pass

    plt.savefig("img/simple.png")

    plt.show()
