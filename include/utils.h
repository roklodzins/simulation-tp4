/**
 * @file utils.h
 * @brief implementation of utilities functions
 * @copyright Romain KLODZINSKI - ISIMA F2 ZZ2 - (c) 2023
 * */

#ifndef UTILS_H
#define UTILS_H


////////////////////////  Includes  ///////////////////////////

#include <cstdio>
#include <cstdint>


////////////////////////   Types   ////////////////////////////

typedef uint8_t u8;
typedef uint16_t u16;
typedef uint32_t u32;
typedef uint64_t u64;

typedef int8_t s8;
typedef int16_t s16;
typedef int32_t s32;
typedef int64_t s64;


////////////////////////   Macros   ///////////////////////////

//#define NO_COLOR 1


/// Define Terminal ANSI Color switch macros
#ifdef NO_COLOR // non-color

#define cprint(R, G, B, ...) printf(__VA_ARGS__)
#define set_fg(R, G, B) (void)0
#define set_bg(R, G, B) (void)0
#define print(...) printf(__VA_ARGS__)
#define reset_color() (void)0
#define RST ""

#else // color

#define RST "\x1b[0m"
#define cprint(R, G, B, ...) printf("\x1b[38;2;" #R ";" #G ";" #B "m" __VA_ARGS__)
#define set_fg(R, G, B) printf("\x1b[38;2;%hhu;%hhu;%hhum", R, G, B)
#define set_bg(R, G, B) printf("\x1b[48;2;%hhu;%hhu;%hhum", R, G, B)
#define print(...) printf(RST __VA_ARGS__)
#define reset_color() print()

#endif


#define H1(...) cprint(255, 0, 0, __VA_ARGS__)
#define H2(...) cprint(0, 172, 127, __VA_ARGS__)
#define H3(...) cprint(200, 135, 0, __VA_ARGS__)
#define H4(...) cprint(0, 80, 245, __VA_ARGS__)
#define Ask(input, ...) H4(__VA_ARGS__); fflush(stdout); (void)scanf("%d", &input)
#define AskUntil(input, cond, ...) do { Ask(input, __VA_ARGS__); } while(cond)


///////////////////////   Functions   /////////////////////////

/**
 * @brief compute the elapsed time, with nanosecond resolution, between 2 calls
 * @return the elapsed time
 * @note the first call may no be trusted
 * */
double time_it();


#endif // UTILS_H
