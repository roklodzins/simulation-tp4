/**
 * @file ObjectSimulation.h
 * @brief implement the ObjectSimulation class
 * */
 

#ifndef TP4_OBJECTSIMULATION_H
#define TP4_OBJECTSIMULATION_H


////////////////////////  Includes  ///////////////////////////

#include <iostream>
#include <list>

#include "Rabbit.h"
#include "Simulation.h"

using namespace std;


/////////////////////////   Class   ///////////////////////////

/**
 * @brief Simulation using individual rabbits representation (slow)
 * */
class ObjectSimulation final : public Simulation {
	using Simulation::Simulation;
	
protected:
    list<Rabbit *>listRabbit;
	
	/**
	 * @brief print all rabbits properties
	 * @note debug
	 * */
    void write();
	
	/**
	 * @brief generate the initial population, randomly spread on all Rings
	 * @param[in] pop the population size
	 * */
	void initiate_population(u64 pop) final;
	
	/**
	 * @brief run 1 month of simulation
	 * */
    void run_month() final;

public:
	/**
	 * @brief clear the rabbits list
	 * */
	~ObjectSimulation() final;
};


#endif //TP4_OBJECTSIMULATION_H
