/**
 * @file PredictSimulation.h
 * @brief implement the PredictSimulation class
 * */

#ifndef TP4_PREDICTSIMULATION_H
#define TP4_PREDICTSIMULATION_H


////////////////////////  Includes  ///////////////////////////

#include "Simulation.h"


////////////////////////    Types   ///////////////////////////

/**
 * @struct SlidingWindow
 * @brief represent a sliding window over a year, used to know the number of births at the corresponding relative month
 * */
typedef struct SlidingWindow {
    u64 slide[12]{0};
    s32 offset = 0; // cpu register size -> faster than u8
} SlidingWindow;


/// Get the corresponding index in the virtual slide from a relative month
#define SlidingWindowIdx(window, idx) ((idx + window.offset) % 12)

/// Get the number of rabbits to born on month 'idx' (relative to now)
#define SlidingWindowAccess(window, idx) window.slide[SlidingWindowIdx(window, idx)]

/// Slide the window by on month (time++)
#define SlidingWindowSlide(window) if (++window.offset == 12) window.offset = 0


/**
 * @struct DeathRings
 * @brief represent a sliding window of rings over a year, used to know the number of deaths at the corresponding relative month, for a specified rabbits group
 * */
typedef struct DeathRings {
	Ring rings[12][5]{{{0}}};
    s32 offset = 0; // cpu register size -> faster than u8
} DeathRings;

/// Get the corresponding rings groups' index in the sliding window by relative month
#define DeathRingsIdx(rings, idx) ((idx + rings.offset) % 12)

/// Get the cell storing the number of deaths prediction for the relative month 'idx' and rabbits group
#define DeathRingsAccess(drings, idx, ring, age) drings.rings[DeathRingsIdx(drings, idx)][ring][age + idx]

/// Increment the number of deaths prediction for the relative month 'idx' and rabbits group
#define DeathRingsRegister(drings, idx, ring, age) DeathRingsAccess(drings, idx, ring, age)++

/// Slide the window by on month (time++)
#define DeathRingsSlide(rings) if (++rings.offset == 12) rings.offset = 0


/////////////////////////   Class   ///////////////////////////

/**
 * @class PredictSimulation
 * @brief refactored version of our first implementation, respecting exactly the litter distribution criteria, at a performance cost.
 * Using a prediction system for births and deaths
 * */
class PredictSimulation final : public Simulation {
	using Simulation::Simulation;
	
protected:
	SlidingWindow prebirth; // births prediction of the following 12 months
	DeathRings predeath;    // deaths prediction of the following 12 months
	
	/**
	 * @brief print the births prediction
	 * @param[in] offseted align the printing on the sliding window offset if true
	 * @note debug
	 * */
	void print_prebirth(bool offseted);
	
	/**
	 * @brief print the deaths prediction
	 * @note debug
	 * */
	void print_predeath();
	
	/**
	 * @brief run 1 month of simulation
	 * */
    void run_month() final;
};

#endif //TP4_PREDICTSIMULATION_H
