/**
 * @file Ring.h
 * @brief implement the Rings system
 * */

#ifndef TP4_RING_H
#define TP4_RING_H


////////////////////////  Includes  ///////////////////////////

#include "utils.h"


////////////////////////    Types   ///////////////////////////

#define RingSize 192 // 12 * 16: (15 + 1) years in months


/**
 * @typedef Ring
 * @brief an u64 array of RingSize
 * */
typedef u64 Ring[RingSize];


/**
 * @struct Rings
 * @brief represent a population of rabbits, by sexe and maturity age.
 * female[i][(u + offset) % RingSize] is the number of female rabbits, which maturity age is i + 4 and current age in month is u
 * @note time is increased (aging all rabbits) by simply decrementing the offset
 * @note maturity is not handled for males
 * @note direct access to both sexe is possible throw the raw rings attribute
 * */
typedef struct Rings {
	union {
		struct { // anonymous struct/union from C standard (GNU ext in C++)
			Ring female[4];
			Ring male;
		};
		Ring rings[5]{{0}};
	};
    s32 offset = 0; // cpu register size -> faster than u8
} Rings;


/// Iter over a ring, searching all rabbits born on the selected month in [0, 11]
#define forRabbitBornOnMonth(date, month, iterator) for (s32 iterator = ((date) - (month)) % 12; iterator < RingSize; iterator += 12)

/// Iter over a ring, searching all rabbits born on the same month as now
#define forRabbitByBornMonth(iterator) for (s32 iterator = 0; iterator < RingSize; iterator += 12)

/// Iter over a ring, searching all rabbits which become mature on the selected month in [0, 11]
#define forRabbitMatureOnMonth(mature, date, month, iterator) forRabbitBornOnMonth(date, month - mature, iterator)

/// Iter over a ring, searching all rabbits which become mature on the same month as now
#define forRabbitByMatureMonth(mature, iterator) for (s32 iterator = mature; iterator < RingSize; iterator += 12)

/// Iter over a ring, searching all rabbits which are adult now
#define forRabbitAdult(mature, iterator) for (s32 iterator = mature; iterator < RingSize; iterator++)

/// Iter over a ring, searching all rabbits which are adult & fertile now
#define forRabbitFertileAdult(mature, limit, iterator) for (s32 iterator = mature; iterator <= limit; iterator++)

/// Iter over a ring, and all rabbits
#define forRabbits(iterator) for (s32 iterator = 0; iterator < RingSize; iterator++)

/// Iter over a ring, searching all rabbits which are children now
#define forRabbitChildren(mature, iterator) for (s32 iterator = 0; iterator < mature; iterator++)

/// Increment age by 1 month for all rabbits
#define RingsAging(rings) if (!rings.offset--) rings.offset += RingSize // loop using positive offset, as C modulo doesn't deal with negatives

/// Get the corresponding index in a ring from an age
#define RingIdx(rings, age) ((age + rings.offset) % RingSize)

/// Get the number of females in ring 'idx' and of age = 'age'
#define RingsFemale(rings, idx, age) rings.female[idx][RingIdx(rings, age)]

/// Get the number of males of age = 'age'
#define RingsMale(rings, age) rings.male[RingIdx(rings, age)]

/// Get the number of rabbits in ring 'idx' and of age = 'age'
#define RingsRaw(rings_, idx, age) rings_.rings[idx][RingIdx(rings_, age)]


#endif //TP4_RING_H
