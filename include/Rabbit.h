/**
 * @file Rabbit.h
 * @brief implement the Rabbit class
 * */
 
#ifndef TP4_RABBIT_H
#define TP4_RABBIT_H


////////////////////////  Includes  ///////////////////////////

#include "Random.h"


/////////////////////////   Class   ///////////////////////////

/**
 * @class Rabbit
 * @brief represent a rabbit
 * */
class Rabbit{

private:
	
    private: u8 age = 0;          // in month
    public: bool female;          // switch btw public/private to keep the same memory layout/padding
    private: u32 predicted_birth; // 12 bits, 1 bit per month, 1 if the rabbit will give birth this month
	// public are intended to be read only from the outside (fast getter)

	
public:
	
	/**
	 * @brief instantiate a Rabbit
	 * @param[in, out] rng the RNG used to generate the rabbit
	 * @param[in] age the age of the rabbit (0 by default)
	 * */
	explicit Rabbit(Random& rng, u8 age = 0);
	
	
	/**
	 * @brief simulate 1 month
	 * @param[in, out] rng the RNG
	 * @return the number of children to give birth to
	 * */
    s32 run_month(Random& rng);
	
	
	/**
	 * @brief test if the rabbit will die this month
	 * @param[in, out] rng the RNG
	 * @return true if dying, false otherwise
	 * */
    bool is_dying(Random& rng) const;
	
	
	/**
	 * @brief print the Rabbit properties
	 * @note debug
	 * */
    void write() const;
};

#endif //TP4_RABBIT_H
