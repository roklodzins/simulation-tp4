/**
 * @file Simulation.h
 * @brief implement the Simulation class
 * */

#ifndef TP4_SIMULATION_H
#define TP4_SIMULATION_H


////////////////////////  Includes  ///////////////////////////

#include "Random.h"
#include "Ring.h"


/////////////////////////   Class   ///////////////////////////

class Simulation {
protected:
    Random random;
	ofstream& out;
	bool internal_file = false;
	
    Rings rings;
	
    u64 date = 0,          // in month
		total_births = 0,  // total on the whole simulation
	    total_deaths = 0,  // total on the whole simulation
		
		births = 0,        // births this month (internal usage)
		deaths = 0,        // deaths this month (internal usage)
		rabbits = 0;       // living rabbits this month
	
	/**
	 * @brief run 1 month of simulation
	 * */
	virtual void run_month();
	
	/**
	 * @brief print the rings, aka the number of rabbits per age & sexe (& maturity for females)
	 * @param[in] offseted align the printing on the ring rotation offset if true
	 * @note debug
	 * */
	void print_rings(bool offseted);
	
	/**
	 * @brief save the simulation state as raw binary format in the output file
	 * @note raw format: Rings rings + u64 total_births + u64 total_deaths + u64 rabbits
	 * */
	void save_state();
	
	/**
	 * @brief generate the initial population, randomly spread on all Rings
	 * @param[in] pop the population size
	 * */
	virtual void initiate_population(u64 pop);
	
public:
	/**
	 * run the simulation
	 * @param[in] years the number of simulated years to run (simulation resolution is month)
	 * @param[in] pop the initial population size (10 by default)
	 * @param[in] silent no log to stdout if true (false by default)
	 * */
	void run(s32 years, u64 pop = 10, bool silent = false);
	
	/**
	 * @brief instanciate a simulation
	 * @param[in] seed the seed used for the RNG
	 * @param[out] out the output file to store each steps states (months)
	 * */
	explicit Simulation(s32 seed, ofstream& out);
	
	/**
	 * @brief instanciate a simulation
	 * @param[in] seed the seed used for the RNG
	 * @param[out] filename the output filename to store each steps states (months)
	 * */
	explicit Simulation(s32 seed, const string& filename);
	
	/**
	 * @brief destroy the simulation and close the output file
	 * */
	virtual ~Simulation();
};

#endif //TP4_SIMULATION_H
