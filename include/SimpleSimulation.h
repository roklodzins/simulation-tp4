/**
 * @file SimpleSimulation.h
 * @brief implement the SimpleSimulation class
 * */


#ifndef TP4_SIMPLESIMULATION_H
#define TP4_SIMPLESIMULATION_H


////////////////////////  Includes  ///////////////////////////

#include "Random.h"


/////////////////////////   Class   ///////////////////////////

/**
 * @class SimpleSimulation
 * @brief implement the basic simulation (the fibo-like one)
 * */
class SimpleSimulation {

private:
	u64 number_adults_females = 1;  // Can have children
	u64 number_growing_females = 0; // Children (females) who grow in one month
	u64 number_males = 1;           // Children + adults males
	
    u64 fibo1 = 3; // these are used for log only (first fibo iters are passed)
    u64 fibo2 = 5;
	
	Random random;
	
	/**
	 * @brief run a month of simulation
	 * */
	void run_month();
	
public:
	
	/**
	 * @brief run the simulation
	 * @param[in] years the number of years to simulate
	 * @param[out] out the output file
	 * */
	void run(s32 years, ostream& out);
	
	/**
	 * @brief instanciate a simulation
	 * @param[in] seed the seed used by the RNG
	 * */
	explicit SimpleSimulation(s32 seed);
};


#endif //TP4_SIMPLESIMULATION_H
