/**
 * @file ThreadPool.h
 * @brief implement the ThreadPool class
 * */

#ifndef TP4_THREADPOOL_H
#define TP4_THREADPOOL_H


////////////////////////  Includes  ///////////////////////////

#include <thread>
#include <queue>

#include "utils.h"
#include "Simulation.h"

using namespace std;


/////////////////////////   Class   ///////////////////////////

/**
 * @class ThreadPool
 * @brief create a thread pool over all available CPU cores to execute simulations of any type
 * */
class ThreadPool {

private:
	
	/**
	 * @struct Task
	 * @brief a task to be executed by a thread
	 * */
	typedef struct {
		Simulation * simu;
		s32 years;
		s32 id;
	} Task;

	
	queue<Task> tasks;  // task queue
	mutex tasks_mutex;  // mutex to protect the task queue while poping
	
	thread * pool;      // the thread pool
	u32 cpu;            // the number of CPU cores available
	
	u32 task_num = 0;   // the number of tasks to be executed
	u32 task_done = 0;  // the number of tasks already executed
	bool first = false; // true if the first task is finished
	
	
	/**
	 * @brief task runner (threads use)
	 * */
	void thread_run();

	
public:
	
	/**
	 * @brief instantiate a ThreadPool
	 * */
	ThreadPool();
	
	
	/**
	 * @brief destroy a ThreadPool
	 * */
	~ThreadPool();
	
	
	/**
	 * @brief push a task to the ThreadPool
	 * */
	void push(Simulation * simu, s32 years, s32 id);
	
	
	/**
	 * @brief wait for all tasks to finish
	 * */
	void wait();
	
	
	/**
	 * @brief run/enable all tasks in the ThreadPool
	 * @param[in] blocking if true, wait for all tasks to finish (see wait)
	 * */
	void run(bool blocking = true);
};

#endif //TP4_THREADPOOL_H
